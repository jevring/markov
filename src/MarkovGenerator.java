import java.io.*;
import java.util.*;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-11-05 17:23:13
 */
public class MarkovGenerator {
    private Map<String, Map<String, Integer>> occurrences = new HashMap<String, Map<String, Integer>>();
    private final Random random = new Random(System.currentTimeMillis());

    public enum WordBoundary {
        Letter, Word
    }
    private final WordBoundary wordBoundary;
    private final int wordLength;
    private int currentWordLength; // this makes this class non-thread-safe

    /**
     * Creates a markov generator with the specified settings.
     * Note that this is <b>NOT</b> thread safe during prepatation.
     * Once prepared, however, multiple threads can access it to create sentences.
     *
     * @param wordBoundary What constitutes a "word" in a "sentence".
     * @param wordLength how many of the specified {@code wordBoundary} constitutes a "word".
     * @param text the text to base the generator on
     */
    public MarkovGenerator(WordBoundary wordBoundary, int wordLength, String text) {
        this.wordBoundary = wordBoundary;
        this.wordLength = wordLength;
        prepare(text);
    }

    private void prepare(String text) {
        for (String sentence : sentences(text)) {
            //System.out.println("sentence: " + sentence);
            String previous = null; // this links every first word in a sentence to null, which means that these words are the only ones that can start new sentences.
            for (String word : words(sentence)) {
                //System.out.println("word: " + word);
                addOccurrence(previous, word);
                previous = word;
            }
        }
        System.out.println(occurrences);
    }

    /**
     * Turns a text into sentences. A sentence ends differently depending on the specified word boundary.
     * If it is {@code Letter} then a sentence ends with punctuation or whitespace.
     * If it is {@code Word} then a sentence ends with "real" sentence punctuation, i.e.: period, questionmark or exclamation mark.
     * @param text the text to be turned into a list of sentences.
     * @return a list of sentences representing the text.
     */
    private List<String> sentences(String text) {
        List<String> sentences = new LinkedList<String>();
        StringBuilder sentence = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            sentence.append(c); // append before we check if it ends a sentence or not. that way we don't throw away punctuation.
            if (endsSentence(c)) {
                final String s = sentence.toString().trim();
                if (s.length() > 0) { // skip " " strings.
                    sentences.add(s);
                    sentence.delete(0, sentence.length());
                }
            }
        }
        // classic mistake. we must add the last word too, if we run out of data. 
        final String s = sentence.toString().trim();
        if (s.length() > 0) { // skip " " strings.
            sentences.add(s);
            sentence.delete(0, sentence.length());
        }
        return sentences;
    }

    private boolean endsSentence(char c) {
        // _todo: add check for wordLength
        // no, actually, that's just when we do the words. These are sentences
        if (wordBoundary == WordBoundary.Letter) {
            return !Character.isLetterOrDigit(c);
        } else {
            return c == '.' || c == '?' || c == '!';
        }
    }

    private List<String> words(String sentence) {
        List<String> words = new LinkedList<String>();
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < sentence.length(); i++) {
            char c = sentence.charAt(i);
            word.append(c); // append before we check if it ends a word or not. that way we don't throw away punctuation.
            if (endsWord(c)) {
                final String w = word.toString().trim();
                if (w.length() > 0) {
                    words.add(w);
                    word.delete(0, word.length());
                }
                currentWordLength = 0;
            }
        }
        // classic mistake. we must add the last word too, if we run out of data. same thing for sentences
        //System.out.println("final word: '" + word.toString() + "'");
        final String w = word.toString().trim();
        if (w.length() > 0) {
            words.add(w);
            word.delete(0, word.length());
        }
        currentWordLength = 0; // otherwise starting words can orten be shorter than the word length
        return words;
    }

    private boolean endsWord(char c) {
        if (wordBoundary == WordBoundary.Letter) {
            return ++currentWordLength >= wordLength;
        } else {
            return Character.isWhitespace(c) && ++currentWordLength >= wordLength;
        }
    }

    private void addOccurrence(String previous, String current) {
        Map<String, Integer> occurrencesForPreviousWord = occurrences.get(previous);
        if (occurrencesForPreviousWord == null) {
            occurrencesForPreviousWord = new HashMap<String, Integer>();
            occurrences.put(previous, occurrencesForPreviousWord);
        }
        Integer occurrence = occurrencesForPreviousWord.get(current);
        if (occurrence == null) {
            occurrence = 0;
        }
        occurrencesForPreviousWord.put(current, occurrence + 1);
    }

    public String generate() {
        String word = null; // start word
        StringBuilder sentence = new StringBuilder();
        while (true) {
            word = newWord(word);
            if (word != null) {
                sentence.append(word);
                if (wordBoundary == WordBoundary.Word) {
                    sentence.append(" ");
                }
                if (endsSentence(word.charAt(word.length() - 1))) {
                    return sentence.toString();
                }
            } else {
                //System.out.println("returned sentence due to finding a finishing word");
                return sentence.toString();
            }
        }
    }

    private String newWord(String current) {
        double r = random.nextDouble();
        //System.out.println("r: " + r);
        Map<String, Integer> nextWords = occurrences.get(current);
        if (nextWords != null) {
            double probability = 0;
            for (Map.Entry<String, Integer> next : nextWords.entrySet()) {
                // the probability of selecting this 'next' among the words possible for this 'current'
                probability += probability(next.getValue(), nextWords.values());
                if (r <= probability) { // note: we can reach over the possible probability here if we roll 1.0 and the rounding screws us
                    return next.getKey();
                }
            }
            System.out.println("could not find a next word for '" + current + "' with roll " + r);
            // note: this should not happen unless our double addition gets screwed up
            return null;
        } else {
            // this word is never followed by another word
            return null;
        }
    }

    private double probability(Integer value, Collection<Integer> integers) {
        // value=5
        // values: 5, 2, 3
        // return: 0.5

        // value=5
        // values: 5, 10, 15
        // return: 5/(15+10+5) = 5/30 = 1/6 = 0.166666666666666666666666667
        int sum = 0;
        for (Integer integer : integers) {
            sum += integer;
        }
        double p = (double)value / (double)sum;
        //System.out.println("p: " + p + " of choosing current word with occurence '" + value + "' out of " + integers);
        return p;
    }

    public static void main(String[] args) {
        InputStreamReader input = null;
        try {
            input = new InputStreamReader(new FileInputStream(args[0]));
            char[] cs = new char[8192];
            int len;
            StringBuilder sb = new StringBuilder();
            while ((len = input.read(cs)) != -1) {
                sb.append(cs, 0, len);
            }
            //MarkovGenerator mg = new MarkovGenerator(WordBoundary.Letter, 2, sb.toString());
            MarkovGenerator mg = new MarkovGenerator(WordBoundary.Word, 1, sb.toString());
            //System.out.println(mg.generate());
            for (int i = 0; i < 25; i++) {
                System.out.println(mg.generate());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
